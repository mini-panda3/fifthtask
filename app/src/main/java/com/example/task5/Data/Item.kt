package com.example.task5.Data

import com.google.gson.annotations.SerializedName

data class Item (
    @SerializedName("street_type", alternate = ["address_type"])
    val addres_type: String,

    @SerializedName("street", alternate = ["address"])
    val addres: String,

    @SerializedName("home_number", alternate = ["house"])
    val house: String,

    @SerializedName("GPS_X", alternate = ["gps_x"])
    val gps_x: String,

    @SerializedName("GPS_Y", alternate = ["gps_y"])
    val gps_y: String)
package com.example.task5.Data

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface GetInfo {

    @GET("./api/atm?city=Гомель")
    fun getATMList() : Observable<List<Item>>

    @GET("./api/infobox?city=Гомель")
    fun getInfoBoxList() : Observable<List<Item>>

    @GET("./api/filials_info?city=Гомель")
    fun getBanksList() : Observable<List<Item>>
}
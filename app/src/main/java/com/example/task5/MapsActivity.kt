package com.example.task5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.example.fourthtask.data.networkConnection
import com.example.task5.Data.GetInfo
import com.example.task5.Data.Item

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.example.task5.databinding.ActivityMapsBinding
import com.google.maps.android.clustering.ClusterManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    lateinit var getInfo: GetInfo
    private val dispose = CompositeDisposable()
    val list = mutableListOf<Item>()
    lateinit var clusterManager: ClusterManager<MarkerCluster>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val gomel = LatLng(52.425163 , 31.015039)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 12.5f))
        clusterManager = ClusterManager(this, mMap)

        val alertBuilder = AlertDialog.Builder(this)

        val networkConnection = networkConnection(applicationContext)
        networkConnection.observe(this, Observer {isConnected ->

            if (isConnected) {
                obtainBankData(gomel)
                mMap.setOnCameraIdleListener(clusterManager)
                mMap.setOnMarkerClickListener(clusterManager)
            } else {
                alertBuilder.setTitle("Map application")
                alertBuilder.setMessage("No internet connection")
                alertBuilder.setNeutralButton("OK"){dialog, i ->
                    dialog.cancel()
                }
                alertBuilder.show()
            }
        })
    }


    private fun obtainBankData (dot : LatLng) {
        var c = 0

        val retrofit = Retrofit.Builder()
            .baseUrl("https://belarusbank.by")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()

        getInfo = retrofit.create(GetInfo::class.java)

        dispose.add(getInfo.getBanksList().mergeWith(getInfo.getInfoBoxList().mergeWith(getInfo.getATMList()))
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ item ->
                item.forEach { i ->
                    list.add(i)
                }
                c++
                if (c == 3)
                    sortItems(list, dot)
            } , {

            }))
    }

    private fun sortItems(list : List<Item>, dot : LatLng){
        var kat1 = 0.0
        var kat2 = 0.0
        var distance = 0.0
        var map = mutableMapOf<Double, Item>()
        var counter = 0

        list.forEach { item ->
            kat1 = abs(dot.latitude - item.gps_x.toDouble())
            kat2 = abs(dot.longitude - item.gps_y.toDouble())
            distance = sqrt(abs(kat1.pow(2.0) - kat2.pow(2.0)))
            map[distance] = item
        }

        for ((d, item) in map.toSortedMap()){
            if (counter < 10){
                val itemOfCluster = MarkerCluster(item.gps_x.toDouble(), item.gps_y.toDouble(), item.addres_type + item.addres + item.house, "")
                clusterManager.addItem(itemOfCluster)
            } else break
            counter++
        }
    }
}